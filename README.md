## Installing a Dependency

### `yarn install`

Alternatively you may use `npm`

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn run test:project`

Snapshot tests and coverage<br>

### `yarn run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

 ## dependencies
    bootstrap:            4.0.0-alpha.6   
    flexiblegs-scss-plus: 5.5.3
    history:              4.6.3
    hover.css:            2.2.0
    normalize.css:        7.0.0
    pixelperfectcss-scss: 1.0.2
    prop-types:           15.5.10
    react:                15.6.1
    react-dom:            15.6.1
    react-router-dom:     4.1.1
  
##  devDependencies 
    enzyme:                    2.9.1
    enzyme-to-json:            1.5.1
    istanbul:                  0.4.5
    react-scripts:             1.0.10
    react-test-renderer:       15.6.1
    stylelint:                 7.12.0
    stylelint-config-standard: 16.0.0