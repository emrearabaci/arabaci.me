import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './views/App';

import 'normalize.css/normalize.css';
import 'bootstrap/dist/css/bootstrap-grid.css';
import 'flexiblegs-scss-plus/css/app.css';
import 'pixelperfectcss-scss/css/app.css';
import 'hover.css/css/hover.css';

ReactDOM.render(
  <App />,
	document.getElementById('root')
);
