import React from 'react';


import twitter from '../img/twitter.svg';
import octocat from '../img/octocat.svg';
import heart from '../img/heart.svg';

import '../styles/Footer.css';

const Footer = () => (
	<footer className="footer">
		<hr className="special"/>
		<br />
		<a
			className="footer-img"
			href="https://twitter.com/JuliusThurinus"
			rel="noopener noreferrer"
			target="_blank">
				<img
				src={twitter}
				width="50"
				height="50"
				alt="emre arabacı twitter"
			/>
		</a>
		<span>&nbsp;&nbsp;</span>
		<a
			className="footer-img"
			href="https://github.com/emrearabaci/"
			rel="noopener noreferrer"
			target="_blank">
			<img
				src={octocat}
				width="45"
				height="50"
				alt="emre arabacı github"
			/>
		</a>
		<br />
		<div className="wrap xl-center">
			<div className="col xl-1-3 lg-1-3 md-hidden sm-hidden">
				<span>&nbsp;</span>
			</div>
			<div className="col xl-1-3 lg-1-3 md-1-1 sm-1-1">
				<span>Emre Arabacı, 2017</span>
			</div>
			<div className="col xl-1-3 lg-1-3 md-1-1 sm-1-1">
				<span>I </span>
				<a
					href="http://flexible.gs/"
					rel="noopener noreferrer"
					target="_blank">
					<img
						src={heart}
						width="15"
						height="15"
						alt="flexible.gs"/>
					<span className="footerText"> Love</span>
				</a>
				<span> Flexible Grid System!</span>
			</div>			
		</div>
		</footer>
);

export default Footer;