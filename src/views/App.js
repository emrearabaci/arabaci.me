import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { BrowserRouter as Router } from 'react-router-dom';
//import createHistory from 'history/createBrowserHistory';

import Header from './Header';
import Footer from './Footer';
import Content from './Content';

import Home from './routes/Home';

//const history = createHistory();

import '../styles/App.css';

class App extends Component {
  render() {
    return (
    <div>
      <Router>
        <div className="App">
          <Header />
          <Content>
            <Switch>
              <Route exact path="/" component={Home} />
            </Switch>
          </Content>
          <Footer />
        </div>
      </Router>
    </div>
    );
  }
}

export default App;
