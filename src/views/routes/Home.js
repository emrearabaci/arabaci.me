import React from 'react';

import '../../styles/Home.css';
import eaprofile from '../../img/eaprofile.jpg';

const Home = () => (
	<div className="home">
		<div className="wrap xl-tac">
			<div className="col xl-1-1">
				<img
					className="img-circle"
					src={eaprofile}
					width="200"
					height="180"
					alt="emre arabacı"
				/>
				<h3>Kim?</h3>
				<p>Emre Arabacı, 1994</p>
				<p>Uludağ Üniversitesi Mekatronik Bölümü mezunuyum. Front-End teknolojileri ile ilgilenmekteyim ve kendimi bu alanda geliştirmeye devam etmekteyim.</p>
				<p>Şu an, yakın zamanda yayınlanacak olan elektronik post-it uygulaması olan pastecenter.com üzerine çalışıyorum.</p>
				<p>İletişim: <span className="red">bilgi@arabaci.me</span></p>
			</div> 
		</div>
	</div>

)

export default Home;