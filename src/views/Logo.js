import React from 'react';

import logo from '../img/logo.svg';

const Logo = () => <img src={logo} width="200" height="100" alt="arabaci.me emre arabacı" />;

export default Logo;