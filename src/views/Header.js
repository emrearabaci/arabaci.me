import React from 'react';

import Logo from './Logo';
import '../styles/Header.css';

const Header = () => (
  <header className="header">
	  <a
	  	className="header-img"
	  	href="http://arabaci.me/"
		rel="noopener noreferrer" 
		target="_self">	
			<Logo />
		</a>
		<hr className="special"/>
	</header>
);

export default Header;