import React from 'react';
import { shallow } from 'enzyme';

import Content from '../views/Content';

it('renders correctly', () => {
  const wrapper = shallow(<Content />);

  expect(wrapper).toMatchSnapshot();
});