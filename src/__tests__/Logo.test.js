import React from 'react';
import { shallow } from 'enzyme';

import Logo from '../views/Logo';

it('renders correctly', () => {
  const wrapper = shallow(<Logo />);

  expect(wrapper).toMatchSnapshot();
});